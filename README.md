# Neural Network For Disparity Refinement
This repo contains different networks for disparity estimation. We have been testing different architecture and different initial algoritmh to compare the performance of the our architecture with the DRR architecture.
We have been using _"FlyingThings"_ and _"KITTI"_ dataset, each branch contains a different architecture optimized for a different dataset and a _".md"_ file with the related results.
The _"KITTI"_ dataset was used for fine tuning operation and, during this fase, the net uses only the existings ground truth data. 